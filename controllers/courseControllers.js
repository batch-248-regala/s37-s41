const Course = require("../models/Course");

module.exports = {
  addCourse: (data) => {
    const newCourse = new Course(data.course);
    newCourse.isAdmin = data.isAdmin;

    return newCourse.save().then((course) => {
      return course;
    });
  },

  getAllCourses: () => {
    return Course.find().then((courses) => {
      return courses;
    });
  },

  getAllActive: () => {
    return Course.find({ isActive: true }).then((courses) => {
      return courses;
    });
  },

  archiveCourse: (courseId, courseData) => {
    return Course.findByIdAndUpdate(courseId, { ...courseData, isActive: false }).then(
      (course) => {
        if (!course) {
          return false;
        } else {
          return true;
        }
      }
    );
  },

  unarchiveCourse: (courseId, courseData) => {
    return Course.findByIdAndUpdate(courseId, { ...courseData, isActive: true }).then(
      (course) => {
        if (!course) {
          return false;
        } else {
          return true;
        }
      }
    );
  },

  updateCourse: (reqParams, reqBody) => {
    const updatedCourse = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price
    };
    //findByIdAndUpdate (document ID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course) => {
      if (!course) {
        return false;
      } else {
        return true;
      }
    });
  }
};
