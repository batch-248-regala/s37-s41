const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result) {
      return true;
    } else {
      return false;
    }
  });
};

//User Registration
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

// User authentication
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (!result) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

//Retreive user details
module.exports.getProfile = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = "";
    return result;
  });
};

module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.enrollments.push({ courseId: data.courseId });
    return user.save().then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  let isCourseUpdated = await Course.findById(data.courseId).then(course => {
    course.enrollees.push({ userId: data.userId });
    return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  // Check if both user and course are updated successfully
  if (isUserUpdated && isCourseUpdated) {
    return true;
  } else {
    return false;
  }
};

