const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
router.post("/", auth.verify, (req, res)=>{
    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    courseController.addCourse(data).then(resultFromController=>res.send(resultFromController));
});

//Route for retrieveing all the courses
router.get("/all", (req, res)=>{
    courseController.getAllCourses().then(resultFromController=>res.send(resultFromController));
});

//Route for retrieving all the ACTIVE courses
router.get("/", (req, res)=>{
    courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
});

router.put("/:courseId", auth.verify, (req, res)=>{
    courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});

router.patch("/:courseId/archive", auth.verify, (req, res) => {
  const courseId = req.params.courseId;
  const courseData = req.body;

  courseController.archiveCourse(courseId, courseData).then((resultFromController) => {
    res.send(resultFromController);
  });
});

router.patch("/:courseId/unarchive", auth.verify, (req, res) => {
  const courseId = req.params.courseId;
  const courseData = req.body;

  courseController.unarchiveCourse(courseId, courseData).then((resultFromController) => {
    res.send(resultFromController);
  });
});


//allows us to export the router object that will be accessed in our index.js file
module.exports = router;
