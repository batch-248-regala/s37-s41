const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth.js");

router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.post("/login", (req, res) => {
  userController.loginUser(req.body).then((resultFromController) =>
    res.send(resultFromController)
  );
});

router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// middleware: auth.verify
// decode jwt: auth.decode function 
router.post("/enroll", auth.verify, (req, res) => {
  // let userData be the object to decode the jwt 
  const userData = auth.decode(req.headers.authorization);
  // create an object called data
  let data = {
    // extract value of the 'id' of the authenticated user from jwt
    userId: userData.id,
    // assign courseId in the req.body
    courseId: req.body.courseId,
  };
  // pass the 'data' object as an argument to the userController.enroll function
  // enroll function -> should update the database by adding the enrolled course to the user's enrollments
  userController.enroll(data).then((resultFromController) => 
    // return the result of the 'enroll' function as a promise then pass to res.send
    res.send(resultFromController));
}); 


module.exports = router;
